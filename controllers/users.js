// [SECTION] Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const auth = require('../auth')
	const Course = require('../models/Course')

// [SECTION] Functionalities [Create]
	module.exports.registerUser = (data) => {
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let mobil = data.mobileNo;
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10),
			mobileNo: mobil
		});
		return newUser.save().then((user, err) =>
			{
				if (user) {
					return user;
				} else {
					return false;
				}
			});
	};

	module.exports.checkEmailExists = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return 'Email Already Exists. Please use another email.';
			} else {
				return 'Email is still available.'
			}
		});
	};

	// Login (User Authentication)
	module.exports.loginUser = (reqBody) => {
		let uEmail = reqBody.email;
		let uPassW = reqBody.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return false;
			} else {
				let passW = result.password;
				const isMatched = bcrypt.compareSync(uPassW, passW);
				if (isMatched === true) {
					let dataNiUser = result.toObject();
					return {access: auth.createAccessToken(dataNiUser)};					
				} else {
					return false;
				};
			};
		});
	};

	module.exports.enroll = async (data) => {
		let id = data.userId 
		let course = data.courseId 
		let isUserUpdated = await User.findById(id).then(user => {
			let coursesEnrolled = [];
			user.enrollments.forEach((info) => 
				coursesEnrolled.push(info.courseId));
			let enrolled = 0
			for (let i = 0; i < coursesEnrolled.length; i++) {
				if (coursesEnrolled[i] === course) {
					enrolled += 1;
					break;
				} else {
					continue;
				}
			};

			if (enrolled === 1) {
				return false;
			} else {
				user.enrollments.push({courseId: course});
				return user.save().then((saved, err) => {
					if (saved) {
						return true;
					} else {
						return false;
					};
				});
			};
		});

		let isCourseUpdated = await Course.findById(course).then(course => {
			let enrolleeList = [];
			course.enrollees.forEach((info) => 
				enrolleeList.push(info.userId));
			let alreadyEnrolled = 0
			for (let i = 0; i < enrolleeList.length; i++) {
				if (enrolleeList[i] === id) {
					alreadyEnrolled += 1;
					break;
				} else {
					continue;
				};
			};
				if (alreadyEnrolled === 1) {
					return false;
				} else {
					course.enrollees.push({userId: id});
					return course.save().then((saved, err) => {
						if (err) {
							return false;
						} else {
							return true;
						};
					});
				}
			});

		if (isUserUpdated && isCourseUpdated) {
			return true 
		} else {
			return 'You have already enrolled in this course.'
		};
	};

// [SECTION] Functionalities [Retrieve]
	module.exports.getProfile = (id) => {
		return User.findById(id).then(user => {
			return user;
		});
	};

// [SECTION] Functionalities [Update]
	// Set User as Admin
	module.exports.setAsAdmin = (userId) => {
		let updates = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
			if (admin) {
				return true;
			} else {
				return 'Updates Failed to be implemented.';
			};
		});
	};

	// Set User as Non-Admin
	module.exports.setAsNonAdmin = (userId) => {
		let updates = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, updates).then((user, err) => {
			if (user) {
				return true;
			} else {
				return 'Failed to set admin as user.';
			};
		});
	};

// [SECTION] Functionalities [Delete]