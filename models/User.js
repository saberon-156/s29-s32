// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Schema
const userBlueprint = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required.']
	},
	email: {
		type: String,
		required: [true, 'Email is Required.']
	},
	password: {
		type: String,
		required: [true, 'Password is Required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required.']
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, 'Course ID is required.']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'enrolled'
			}
		}
	]

});

// [SECTION] Model
module.exports = mongoose.model("User", userBlueprint);

